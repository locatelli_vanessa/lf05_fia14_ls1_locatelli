﻿import java.util.Scanner;
import java.io.PrintStream;

class Fahrkartenautomat {
	int value;

	public static void main(String[] args) {

		double ticketpreis = 2.50;
		// System.out.print("Ticket Preis: ");
		// System.out.printf("%.2f", ticketpreis);
		// System.out.print("(Euro);\n");

		double zuZahlenderBetrag = fahrkartenbestellungErfassen();

		if (zuZahlenderBetrag > 0) {
			// Geldeinwurf
			double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			// Fahrscheinausgabe
			fahrkartenAusgeben();
			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
		}

	}

	/**
	 * Method warten die Input von Benutzer.
	 * 
	 * @return Nummer die Fahrkarte was hat die Benutzer gescrieben
	 */
	public static double fahrkartenbestellungErfassen() {
		double TicketPreiseins = 2.90;
		double TicketPreisZwei = 8.60;
		double TicketPreisDrei = 23.50;

		double ticketPreisFinal = 0.0;
		Scanner tastatur = new Scanner(System.in);
		System.out.printf("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus: \n\ninzelfahrschein Regeltarif AB ["
				+ TicketPreiseins + "EUR] (1)\r\n" + "Tageskarte Regeltarif AB [" + TicketPreisZwei + "EUR] (2)\r\n"
				+ "Kleingruppen-Tageskarte Regeltarif AB [" + TicketPreisZwei + "EUR] (3)\r\n");
		int Wunschfahrkarte = tastatur.nextInt();
		System.out.print("Ihre Wahl: " + Wunschfahrkarte);
		System.out.print("\nAnzahl der Tickets: ");
		int anzahlderTickets = tastatur.nextInt();
		if (anzahlderTickets > 10) {

			System.out.print(">> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
			fahrkartenbestellungErfassen();
		}

		System.out.print("Anzahl der Tickets: " + anzahlderTickets);

		if (Wunschfahrkarte == 1 && anzahlderTickets <= 10) {

			ticketPreisFinal = anzahlderTickets * TicketPreiseins;

		}
		if (Wunschfahrkarte == 2 && anzahlderTickets <= 10) {

			ticketPreisFinal = anzahlderTickets * TicketPreisZwei;

		}
		if (Wunschfahrkarte == 3 && anzahlderTickets <= 10) {

			ticketPreisFinal = anzahlderTickets * TicketPreisDrei;

		} else if (Wunschfahrkarte > 3) {
			System.out.print(" >>falsche Eingabe<< ");
		}

		return ticketPreisFinal;
	}

	public static double fahrkartenBezahlen(double zuZahlen) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlen) {
			System.out.println("\r\n Noch zu zahlen: " + (zuZahlen - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {

		System.out.println("\n\n");

		// Rückgeldberechnung und -Ausgabe
		// -------------------------------
		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");

	}
}
